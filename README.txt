Maintenance URLs module allows you to specify several URLs that should temporarily
redirect to a maintenance page. This is a very basic and light weight module.

The maintenance page is a standard Drupal menu hook that renders a translatable string.

The primary user (uid = 1) will ignore these settings, and display a warning (for debugging purposes).

## INSTALLATION ##

1. Enable module as usual.
2. Go to /admin/config/development/maintenance-urls.
3. Configure settings as required.
